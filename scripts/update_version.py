import os, json

user_path = os.path.expanduser("~")
# just dont want to change the vars lol
cf_path = user_path + "/AppData/Roaming/PrismLauncher/instances/Vanilla Resewn/.minecraft/"
title_screen_path = cf_path + "config/isxander-main-menu-credits.json"
warning_path = cf_path + "config/fabric_loader_dependencies.json"

def load_json(path):
    return json.load(open(path))

def save_file(path, obj):
    with open(path, "w") as f:
        json.dump(obj, f, separators=(',', ':'))

title_screen_obj = load_json(title_screen_path)
existing_version = title_screen_obj["main_menu"]["bottom_right"][0]["text"]

print("Current version: " + existing_version)
new_version = input("Enter new version: Vanilla Resewn ")

title_screen_obj["main_menu"]["bottom_right"][0]["text"] = "Vanilla Resewn " + new_version
save_file(title_screen_path, title_screen_obj)

warning_file_obj = load_json(warning_path)
try:
    warning_file_obj["overrides"]["fabric-api"]["+recommends"]["Vanilla Resewn"] = ">" + new_version
except KeyError:
    warning_file_obj["overrides"]["fabric"]["+recommends"]["Vanilla Resewn"] = ">" + new_version
save_file(warning_path, warning_file_obj)