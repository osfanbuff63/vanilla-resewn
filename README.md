# Vanilla Resewn

## About

<img alt="Built on Fabulously Optimized" height="40" src="https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@2/assets/compact/built-with/fabulously-optimized_vector.svg">

This modpack for Minecraft tries to stay true to the vanilla style, while still providing some useful tools.

## Download

<a href="https://modrinth.com/modpack/vanilla-resewn"><img alt="Download on Modrinth" height="40" src="https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@2/assets/compact/available/modrinth_vector.svg"></a>
<a href="https://codeberg.org/osfanbuff63/vanilla-resewn/releases"><img alt="Download on Codeberg" height="40" src="https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@2/assets/compact/available/codeberg_vector.svg"></a>

## Help and general chat

<a href="https://discord.gg/aKZxB6xTYZ"><img alt="discord-singular" height="40" src="https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@2/assets/compact/social/discord-singular_vector.svg"></a>

Issues should be reported on [Codeberg](https://codeberg.org/osfanbuff63/vanilla-resewn/issues).

## FAQ

> Q: Did you make this all yourself or is it based on another modpack?

A: This modpack is based on the **awesome** [Fabulously Optimized](https://modrinth.com/modpack/fabulously-optimized) modpack, and will stay updated with it, like a soft fork.

> Q: Can I make a fork of this pack?

A: Absolutely! You must follow the terms of the [BSD 3-Clause License](LICENSE.md), but otherwise you can do pretty much anything!

## License

This modpack is licensed under the [BSD-3-Clause License](LICENSE.md).
